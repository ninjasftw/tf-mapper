# Terraform Mapper

This python script helps to make sense of complicated Terraform projects that often use dependent modules.

It will pull in all the dependencies of a project (from git and local filesystem) and can then display it as a GraphViz diagram or a stdout text list.

By default it will display only module dependencies however you can also change the detail level so that it reports on resources.

It currently only supports pulling down Git repos via ssh and will automatically convert a https repo reference to ssh.

## How to use

```
python3 tf_mapper.py --parent_module=git@github.com:org/project.git
```

### Getting help
There are a number of command options that can be used by passing in --help

```
  --parent_module TEXT           location of module to start at
  --initial_filename TEXT        Only scan a particular file in in the parent module.  Supports wildcards
  --detail_level INTEGER         Detail level to trace through.  Set to 2 to
                                 display resource components.
  --refresh_repo BOOLEAN         Whether to refresh remote repos.  Defaults to
                                 true
  --graph_engine TEXT            Engine to use when generating the graph.
                                 Options are dot (default), neato, fdp, sfdp,
                                 circo, twopi, osage
  --disable_graph BOOLEAN        Whether to disable the graph generation
  --print_relationships BOOLEAN  Whether to print out the relationship map
  --log_level TEXT
  --scratch_dir TEXT             Location to store repos
  --git_checkout_user TEXT       If checking out dependent modules from git,
                                 which user to checkout as
  --help                         Show this message and exit.
```


## Example

Basic run
```
python3 tf_mapper.py --parent_module=git@github.com/wardviaene/terraform-course.git --git_checkout_user=git --initial_filename=*/module-demo/*
```

Produces
[![Basic run](./images/terraform-course.git.png)](./images/terraform-course.git.png)


Enable detailed
```
python3 tf_mapper.py --parent_module=git@github.com/wardviaene/terraform-course.git --git_checkout_user=git --initial_filename=*/module-demo/* --detail_level=2
```
[![Detailed run](./images/terraform-course_detailed.git.png)](./images/terraform-course_detailed.git.png)


## Development

This project is still under active development and there are a few high priority issue including
   - Url filters only realy work against github repos currently
   - Detailed mapping only includes resource types.  Need to add options for Providers, Data, Variables etc
   - There are some recursion issues where modules that have circular references can get stuck

# purpose track dependicies of TF modules and produce a csv that can be imported into draw.io
# highlighting individual variables etc is going to make it difficult to read.
# would be call if calls to AWS components put that component in the diagram. eg ec2, vpc, msk.  would subnets be too much
# if it finds a git repo it will pull it down to a temp dir
# 


import hcl2
from os import walk
import subprocess
import pydot
import click
import re
import os.path
import logging
import fnmatch


def create_module_name(module_location: str) -> str:
    if module_location.startswith("/"):
        return module_location.split("/")[-1]
    elif module_location.startswith("git::ssh") or module_location.startswith("git::https"):
        return re.sub('\.git$', '', module_location.split("?")[0].split("/")[-1])
    elif module_location.startswith("github.com") or module_location.startswith("git@github.com"):
        return module_location.split("/")[-1].split("?")[0]        
    else:
        # if we've got here then chances are its a sub module dependency with a local reference i.e parent_dir/submodule/module
        return module_location.split("?")[0]     

def check_tf_module(module_location: str, mapping_dict: dict, relationship_list: list, detail_level: int, parent_location: str, initial_filename: str) -> tuple[dict,list]:
    """
    Module to parse a 

    Args:
    module_location (str) - location (git or local filesystem) of repo to start with
    mapping_dict (Dict) - Dictionary containing modules 
    relationship_list (list) - list of relationships between modules
    parent_location (str) - Initial module name.  Used to to identify the root node 
    initial_filename (str) - Used to spcify a file to filter in the parent module.

    Returns:
    Dict - dictionary of all discovered TF modules so far
    """
    logging.info(f"Processing module in {module_location}")    
    
    global refresh_repo_global
    global scratch_dir_global
    global git_checkout_user_global
    
    hcl_contents = ""
    default_module_name = create_module_name(module_location)
    logging.debug(f"Default module name -> {default_module_name}")
    logging.debug(f"parent_location -> {parent_location}")
    #determine type of module
    if module_location.startswith("/"):
        module_type = "local"
        local_location = module_location
    elif module_location.startswith("git::") or module_location.startswith("github.com") or module_location.startswith("git@github.com"):
        logging.debug("module is git repo so clone it in")
        module_type = "gitssh"    
        
        local_location=scratch_dir_global + default_module_name

        # Note: There are a number of different ways the repos link to a dependent module so basically assume nothing, strip 
        # as much as possible and add it in manually.
        module_subpath = None

        # Start building the git clone url
        raw_repostring = re.search("[\w]+\.\w+[:|/]([\w-]*(/)*)*", module_location).group()

        # to deal with scenarios where the format is combination : and / delimited 
        if re.search("[\w]+\.\w+\/", raw_repostring):
            raw_repostring = raw_repostring.replace("/", ":", 1)

        # some repos have a module dependency on 
        if len(raw_repostring.split("/")) > 2:
            logging.debug(f"Possible submodule scenario in {raw_repostring.split('/')}")
            module_subpath = "/".join(raw_repostring.split('/')[2:])
            raw_repostring = raw_repostring.split('/')[0] + "/" + raw_repostring.split('/')[1]

        raw_repostring = re.sub(r'\.git$', '', raw_repostring) + ".git"

        #check if repo has already be checked out to scratch dir
        if refresh_repo_global:
            if not os.path.isdir(local_location + "/.git"):
                process = subprocess.Popen(['git', 'clone', git_checkout_user_global + "@" + raw_repostring, local_location], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                with process.stdout:
                    for line in iter(process.stdout.readline, b''):
                        logging.info("Git clone subprocess: %s" % line.decode("utf-8").strip())
                process_Exit_code = process.wait()
                if process_Exit_code != 0:
                    logging.error("An error occurred during git clone")

            if (len(module_location.split("?"))) > 1:
                # fetch tags
                process = subprocess.Popen(['git', 'fetch', '--all', '--tags'], cwd=local_location, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                with process.stdout:
                    for line in iter(process.stdout.readline, b''):
                        logging.debug("Git fetch subprocess: %s" % line.decode("utf-8").strip())
                process_Exit_code = process.wait()
                if process_Exit_code != 0:
                    logging.error("An error occurred during git fetch")                

                # checkout tag
                process = subprocess.Popen(['git', 'checkout', 'tags/' + module_location.split("=")[1]], cwd=local_location, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                with process.stdout:
                    for line in iter(process.stdout.readline, b''):
                        logging.debug("Git checkout subprocess: %s" % line.decode("utf-8").strip())
                process_Exit_code = process.wait()
                if process_Exit_code != 0:
                    logging.error("An error occurred during git checkout")

        if not module_subpath == None:
            local_location = local_location + "/" + module_subpath
    else:
        local_location=scratch_dir_global + parent_location + "/" + default_module_name

    for root, _, files in os.walk(local_location):
        for hcl_file in files:
            if hcl_file.endswith(".tf"):
                if not initial_filename or fnmatch.fnmatch(os.path.join(root, hcl_file), initial_filename):
                    logging.info("Processing file %s" % os.path.join(root, hcl_file))
                    with open(os.path.join(root, hcl_file), 'r') as fp:
                        hcl_contents = hcl_contents + fp.read() + "\n"

    hcl_obj = hcl2.loads(hcl_contents)

    # if there isn't a locals or locals/name then we have to try and guess it
    if not "locals" in hcl_obj or not "name" in hcl_obj['locals']:
        if not "locals" in hcl_obj:
            hcl_obj['locals'] = []

        # hcl_obj['locals']['name'] = default_module_name
        local_name = {"name": default_module_name} 
        hcl_obj['locals'].append(local_name)

    if not default_module_name in mapping_dict:
        mapping_dict[default_module_name] = hcl_obj
        if parent_location == default_module_name:
            logging.debug("Setting node type to circle")
            mapping_dict[default_module_name]['item_type'] = "ellipse"    
        else:
            mapping_dict[default_module_name]['item_type'] = "box"

        if initial_filename:
            mapping_dict[default_module_name]['label'] = f"{default_module_name}\n{module_location}\nFilter={initial_filename}"
        else:
            mapping_dict[default_module_name]['label'] = f"{default_module_name}\n{module_location}"

    # look for modules in the hcl_obj.  
    if "module" in hcl_obj:
        for module_dict in hcl_obj["module"]:
            module_name = next(iter(module_dict))
            logging.info(f"dependent module found {module_name} -> {module_dict[module_name]['source']}")

            logging.debug(f"Dependent module contains {module_dict}")
            # modules are a dict so iterate through them

            for module_item in module_dict:
                relationship_list.append(f"{default_module_name}->{create_module_name(module_dict[module_item]['source'])}")
                if not create_module_name(module_dict[module_item]['source']) in mapping_dict:
                    logging.debug(f"module {create_module_name(module_dict[module_item]['source'])} not in mapping_dict so need to check it out")
                    mapping_dict, relationship_list = check_tf_module(module_dict[module_item]['source'], mapping_dict, relationship_list, detail_level, default_module_name, None)
    else:
        logging.debug(f"no dependent module found in hcl_obj {default_module_name}")

    if "resource" in hcl_obj and detail_level >1:
        for resource_item in hcl_obj['resource']:
            resource = next(iter(resource_item))
        
            if not default_module_name + "/" + resource in mapping_dict:
                mapping_dict[default_module_name + "/" + resource] = {'item_type': 'hexagon',
                                                                    'label': f"resource\n{resource}"}

            relationship_list.append(f"{default_module_name}->{default_module_name}/{resource}")


    # if "provider" in hcl_obj:
    #     print("TBC")

    logging.info(f"Finished Processing module {module_location}")
    return mapping_dict, relationship_list

@click.command()
@click.option('--parent_module', help='location of module to start at')
@click.option('--initial_filename', default=None, help='Only scan a particular file in in the parent module. Supports wildcards')
@click.option('--detail_level', default=1, help='Detail level to trace through.  Set to 2 to display resource components.')
@click.option('--refresh_repo', default=True, help="Whether to refresh remote repos.  Defaults to true")
@click.option('--graph_engine', default="dot", help="Engine to use when generating the graph.  Options are dot (default), neato, fdp, sfdp, circo, twopi, osage")
@click.option('--disable_graph', default=False, help="Whether to disable the graph generation")
@click.option('--print_relationships', default=False, help="Whether to print out the relationship map")
@click.option('--log_level', default="INFO")
@click.option('--scratch_dir', default="./scratch/", help="Location to store repos")
@click.option('--git_checkout_user', default="git", help="If checking out dependent modules from git, which user to checkout as")
def tf_mapper(parent_module, initial_filename, detail_level, refresh_repo, graph_engine, disable_graph, print_relationships, log_level, scratch_dir, git_checkout_user):
    global refresh_repo_global
    global scratch_dir_global
    global git_checkout_user_global

    refresh_repo_global = refresh_repo
    scratch_dir_global = scratch_dir
    git_checkout_user_global = git_checkout_user

    logging.basicConfig(level=getattr(logging, log_level.upper(), None),format='%(levelname)s:%(message)s',)

    logging.info('Starting Terraform mapping')

    # Lets start the recursion! God help us all!
    mapping_dict, relationship_list = check_tf_module(parent_module, {}, [], detail_level, create_module_name(parent_module), initial_filename)

    # Create a new pydot graph object
    new_graph = pydot.Dot("TerraformGraph", graph_type="digraph")

    processed_items = []
    processed_relationships = []
    if print_relationships:
        logging.info("Displaying relationships")    
    for edge_item in relationship_list:
        edge_source = edge_item.split("->")[0]
        edge_dest = edge_item.split("->")[1]
        if not edge_source in processed_items:
            new_graph.add_node(pydot.Node(edge_source, shape=mapping_dict[edge_source]['item_type'], label=mapping_dict[edge_source]['label']))
            processed_items.append(edge_source)
        if not edge_dest in processed_items:
            new_graph.add_node(pydot.Node(edge_dest, shape=mapping_dict[edge_dest]['item_type'], label=mapping_dict[edge_dest]['label']))
            processed_items.append(edge_dest)        

        # TODO: this should probably be done when adding relationships originaly
        if not edge_source + "->" + edge_dest in processed_relationships:
            new_graph.add_edge(pydot.Edge(edge_source, edge_dest))
            if print_relationships:
                logging.info("%s -> %s" % (edge_source, edge_dest))
            processed_relationships.append(edge_source + "->" + edge_dest)

    if print_relationships:
        logging.info("End of relationships")    


    if not disable_graph:
        logging.info("Generating Graph to %s" % create_module_name(parent_module) + '.png')  
        new_graph.write_png(create_module_name(parent_module) + '.png', prog=graph_engine)

    logging.info('Done')

if __name__ == '__main__':
    tf_mapper()